#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
# Created By  : Ellis Thompson
# Created Date: 26 July 2022
# version ='0.2'
# ---------------------------------------------------------------------------
""" Extra functions used globally"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

from os import getenv
from typing import List

import numpy as np
import scipy as sp
import yaml
from dotenv import load_dotenv

load_dotenv()

def dict_to_list(dictionary: dict, keys: List[str]) -> np.ndarray:
    """ Converts a dictionary into a list from predefined keys.

    Args:
        dictionary (dict): The dictionary to convert.
        keys (List[str]): The keys to convert.
    Returns:
        np.ndarray: The converted list.
    """
    
    return np.array([dictionary[key] for key in keys])


def get_centre(_set: np.ndarray) -> np.ndarray:
    """Gets the centre of a set of points

    Args:
        _set (np.ndarray): The initial set of points.

    Returns:
        np.ndarray: Returns the centre points.
    """
    
    return np.mean(_set, axis=0)

 
def compute_radii(training_traces: np.ndarray, centre_trace: np.ndarray) -> np.ndarray:
    """Computes the radii of a set of training traces.

    Args:
        training_traces (np.ndarray): The training traces.
        centre_trace (np.ndarray): The centre trace.

    Returns:
        np.ndarray: Returns the radii.
    """
    
    initial_radii = [0.0] * (len(centre_trace[0]) - 1)
    for trace in training_traces:
        for j in range(1, len(initial_radii) + 1): # Calculate the initial radii ignoring the time
            initial_radii[j - 1] = max(initial_radii[j - 1], abs(centre_trace[0][j] - trace[0][j]))
    return np.array(initial_radii) + float(getenv('EPSILON'))

def get_std(_set: np.ndarray) -> float:
    """Gets the standard deviation of a set of points

    Args:
        _set (np.ndarray): The initial set of points.

    Returns:
        float: Returns the standard deviation.
    """
    
    return np.std(_set, axis=0, ddof=1)


def parse_yaml(file: str) -> tuple[dict, dict]:
    """Parses a yaml file into a dictionary

    Args:
        file (str): The file to parse.

    Returns:
        Tuple([dict, dict]): Returns the parsed dictionaries as the `initial_conditions` and `route`.
        
    Raises:
        FileNotFoundError: If the file is not found after trying both `.yml` and `.yaml`.
    """
    
    origional_path = file
    file = file.replace('.yaml', '.yml')
    
    
    try:
        with open(file, 'r') as stream:
            _yaml_dict = yaml.safe_load(stream)
            initial_conditions = _yaml_dict['initial']
            route = _yaml_dict['route']

            return initial_conditions, route
    except FileNotFoundError:
        try:
            file.replace('.yml','.yaml')

            with open(file, 'r') as stream:
                _yaml_dict = yaml.safe_load(stream)
                initial_conditions = _yaml_dict['initial']
                route = _yaml_dict['route']

                return initial_conditions, route
        except FileNotFoundError as e:
            raise FileNotFoundError(f'File {origional_path} not found') from e
