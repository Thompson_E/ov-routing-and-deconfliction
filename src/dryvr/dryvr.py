#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
# Created By  : Ellis Thompson
# Created Date: 27 July 2022
# version ='0.2'
# ---------------------------------------------------------------------------
""" Main caller for all DryVR code"""

# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

import numpy as np
from src.functions import compute_radii
from src.dryvr.functions import discrepancy_params, normalise_radii


class DryVR:
    
    @classmethod
    def dryvr_run(cls, training_traces: np.ndarray, centre_idx: int = 0) -> np.ndarray:
        """Main caller for computing a reachtube

        Args:
            training_traces (np.ndarray): A list of training traces.
            centre_idx (int, optional): The index of the centre trace. Defaults to 0.

        Returns:
            np.ndarray: Returns a compiled list of reachtubes.
        """
        
        # Assert that the training traces are valid
        if len(training_traces) < 2:
            raise ValueError("There must be at least 2 training traces.")
        
        # Get the centre trace
        centre_trace = training_traces[centre_idx]
        
        # Compute the initial radii and discrepancy parameters
        initial_radii = compute_radii(training_traces, centre_trace)
        d_params = discrepancy_params(training_traces, initial_radii)
        
        result = cls.compute_reachtube(centre_trace, initial_radii, d_params, method = 'PWGlobal')
        
        # assert 100% training accuracy (all trajectories are contained)
        for trace_ind in range(training_traces.shape[0]):
            if not (np.all(result[:, 0, :] <= training_traces[trace_ind, 1:, :]) and
                    np.all(result[:, 1, :] >= training_traces[trace_ind, 1:, :])):
                assert not np.any(np.abs(training_traces[trace_ind, 0, 1:] - centre_trace[0, 1:]) > initial_radii)
                print("Warning: Trace #%d" % trace_ind,
                    "of this initial set is sampled outside of the initial set because of floating point error"
                    " and is not contained in the initial set")
        return result
    
    @classmethod    
    def compute_reachtube(cls, centre_trace: np.ndarray, initial_radii: np.ndarray, d_params: np.ndarray, method:str = 'PWGlobal') -> np.ndarray:
        """Computes a reachtube for a set of traces, radii and parameters.

        Args:
            centre_trace (np.ndarray): The centre trace.
            initial_radii (np.ndarray): Array of initial radii.
            d_params (np.ndarray): The descrepency parameters.
            method (str, optional): Method to use. Defaults to 'PWGlobal'.

        Returns:
            np.ndarray: Returns the array reachtube
        """
        
        normalised_initial_radii = normalise_radii(initial_radii)
        
        trace_len, num_dims = centre_trace.shape # This includes time
        
        if method == 'PWGlobal':
            df = np.zeros((trace_len, num_dims))
            alldims_linear_separators = d_params
            num_dims = centre_trace.shape[1]
            for dim_ind in range(1, num_dims):
                prev_val = 0
                prev_ind = 1 if initial_radii[dim_ind - 1] == 0 else 0
                linear_separators = alldims_linear_separators[dim_ind - 1]
                if initial_radii[dim_ind - 1] != 0:
                    df[0, dim_ind] = initial_radii[dim_ind - 1]
                for linear_separator in linear_separators:
                    _, _, slope, y_intercept, start_ind, end_ind = linear_separator
                    assert prev_ind == start_ind
                    assert start_ind < end_ind
                    segment_t = centre_trace[start_ind:end_ind + 1, 0]
                    segment_df = normalised_initial_radii[dim_ind - 1] * np.exp(y_intercept + slope * segment_t)
                    segment_df[0] = max(segment_df[0], prev_val)
                    df[start_ind:end_ind + 1, dim_ind] = segment_df
                    prev_val = segment_df[-1]
                    prev_ind = end_ind
        else:
            print('Discrepancy computation method,', method, ', is not supported!')
            raise ValueError
        assert (np.all(df >= 0))
        reachtube_segment = np.zeros((trace_len - 1, 2, num_dims))
        reachtube_segment[:, 0, :] = np.minimum(centre_trace[1:, :] - df[1:, :], centre_trace[:-1, :] - df[:-1, :])
        reachtube_segment[:, 1, :] = np.maximum(centre_trace[1:, :] + df[1:, :], centre_trace[:-1, :] + df[:-1, :])

        return reachtube_segment