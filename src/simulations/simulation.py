#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
# Created By  : Ellis Thompson
# Created Date: 26 July 2022
# version ='0.2'
# ---------------------------------------------------------------------------
""" A parent class for all simulations"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

from os import getenv
from typing import List

import numpy as np
from dotenv import load_dotenv

load_dotenv()
# Threashold for the minimum time horizon

class Simulation:
    def __init__(self, horizon: int = 40, dt: float = 1.0, n: int = 1000):
        """Initilise the simulation class for quick reuse.

        Args:
            horizon (int, optional): The time horizon that the simulation should project to (seconds). Defaults to 40.
            dt (float, optional): How long one timestep should progress the simulation by (seconds). Defaults to 1.0.
            n (int, optional): The number of simulation traces to generate. Defaults to 1000.
        """

        self.horizon = horizon
        self.dt = dt
        self.n = n
        
    @property
    def dt(self):
        return self._dt

    @dt.setter
    def dt(self, value):
        self._dt = value
        
    @property
    def horizon(self):
        return self._horizon

    @horizon.setter
    def horizon(self, value):
        if value < float(getenv("THREASHOLD")):
            raise ValueError(f'The time horizon value must be greater than {getenv("THREASHOLD")} seconds.')
        
        self._horizon = value
    
    @property
    def n(self):
        return self._n
    
    @n.setter
    def n(self, value):
        if value < 10:
            raise ValueError("The number of simulation traces must be greater than 10.")
        
        self._n = value  
    
    def initilise_and_run(self):
        raise NotImplementedError("This method must be implemented by the child class.")
    
    def initilise(self):
        raise NotImplementedError("This method must be implemented by the child class.")
    
    def run(self):
        raise NotImplementedError("This method must be implemented by the child class.")
    
    def step(self):
        raise NotImplementedError("This method must be implemented by the child class.")
    
    def reset(self):
        raise NotImplementedError("This method must be implemented by the child class.")
