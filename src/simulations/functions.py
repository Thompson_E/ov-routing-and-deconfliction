#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
# Created By  : Ellis Thompson
# Created Date: 02 August 2022
# version ='0.2'
# ---------------------------------------------------------------------------
""" Functions used as part of the simulation environments."""

# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

from typing import List

import numpy as np


def generate_initial_points(mu: List[float], std: List[float], n: int) -> np.ndarray:
        """Generate n initial points for the simulation (n+1 including the centre), using a normal distribution.
        
        Args:
            mu (List[float]): A list of means for the initial conditions.
            std (List[float]): A standard deviation for the initial conditions.
            n (int): The number of initial points to generate.

        Returns:
            np.ndarray: Returns an array of starting points in the form [lat, lon, alt, hdg, spd].
        """
        
        # Generate n initial points
        points = np.random.normal(mu, std, size = (n, 5))
        # Find the centre of the initial points
        centre = np.array([mu.copy()])
        
        return np.append(centre, points, axis=0)  
