#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
# Created By  : Ellis Thompson
# Created Date: 26 July 2022
# version ='0.2'
# ---------------------------------------------------------------------------
""" A series of function used for generating n simulation traces using bluesky as the simulation approach"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

from os import getenv
from typing import List

import bluesky as bs
import numpy as np
from bluesky.simulation import ScreenIO
from dotenv import load_dotenv
from src.simulations.simulation import Simulation
from tqdm import tqdm

load_dotenv()

# Conversion for feet to metres (f/3.281)
m = float(getenv("m"))
# Conversion for knot to m/s (kts/1.944)
ms = float(getenv("ms"))

class ScreenDummy(ScreenIO):
    """
    Dummy class for the screen. Inherits from ScreenIO to make sure all the
    necessary methods are there. This class is there to reimplement the echo
    method so that console messages are printed.
    """
    
    def __init__(self, verbose = False):
        """Initilising the dummy screen

        Args:
            verbose (bool, optional): If BlueSky messages should be displayed. Defaults to False.
        """
        
        super().__init__()
        self.verbose = verbose
    
    def echo(self, text='', flags=0):
        """Just print echo messages"""
        
        if self.verbose:
            print("BlueSky console:", text)


class Sim(Simulation):
    def __init__(self, horizon: int = 40, dt: float = 1.0, n: int = 500):
        """Initilise the simulation class for quick reuse.

        Args:
            horizon (int, optional): The time horizon that the simulation should project to (seconds). Defaults to 40.
            dt (float, optional): How long one timestep should progress the simulation by (seconds). Defaults to 1.0.
            n (int, optional): The number of simulation traces to generate. Defaults to 1000.
        """
        
        super().__init__(horizon, dt, n)
        
        bs.init(mode ='sim', detached = True)   
        bs.scr = ScreenDummy()


    def initilise_and_run(self, initial_points: np.ndarray, wpts: List[List[float]] = None) -> np.ndarray:
        """Initilise a simulation by selecting starting points using a normal distribution and then automatically run that sim.

        Args:
            initial_points (np.ndarray): Array of initial starting points for the aircraft.
            wpts (List[List[float]], optional): A series of waypoints in the form [lat, lon, alt, (spd)]. Defaults to None.
            
        Returns:
        np.ndarray: Returns an array of each trace for all time t.
        """
        
        self.initilise(initial_points, wpts)
        
        return self.run()


    def initilise(self, initial_points: np.ndarray, wpts: List[List[float]] = None):
        """Initilise a simulation by selecting starting points using a normal distribution.

        Args:
            initial_points (np.ndarray): Array of initial starting points for the aircraft.
            wpts (List[List[float]], optional): A series of waypoints in the form [lat, lon, alt, (spd)]. Defaults to None.
        """
        
        # Add the aircraft to the simulation
        self.add_aircraft(initial_points, wpts)


    def run(self) -> np.ndarray:
        """Run the simulation.
        
        Returns:
            np.ndarray: Returns an array of the simulation traces.
        """
        
        
        traces = [self.state]
        
        for _ in tqdm(range(self.horizon)):
            bs.sim.step()
            traces.append(self.state)
            
        return np.array(traces)


    def add_aircraft(self, initial_points: np.ndarray, wpts: List[dict] = None):
        """Add aircraft to the simulation.

        Args:
            initial_points (np.ndarray): Array of initial starting points for the aircraft.
            wpts (List[dict], optional): An array of waypoints. Defaults to None.
        """
        
        if wpts is None:
            wpts = []
        
        for c, init in enumerate(initial_points):
            _id = f"AC{c}"
            
            # Create an aircraft with the initial state
            bs.traf.cre(acid=f"{_id}", actype="AMZN", aclat=init[0], 
                aclon=init[1], achdg=[init[3]], acalt=init[2]/m, 
                acspd=init[4]/ms)
            
            for wpt in wpts: # add the target waypoints
                if len(list(wpt.keys())) == 3: # Waypoint without speed
                    bs.stack.stack(f'ADDWPT {_id} {wpt["lat"]} {wpt["lon"]} {wpt["alt"]}')
                else: # Waypoint with speed
                    bs.stack.stack(f'ADDWPT {_id} {wpt["lat"]} {wpt["lon"]} {wpt["alt"]} {wpt["spd"]}')
                    
            bs.stack.stack(f'LNAV {_id} ON; VNAV {_id} ON') # turn on VNAV


    @property
    def state(self) -> np.ndarray:
        """Get the state of the simulation.

        Returns:
            np.ndarray: Returns an array of the state of the simulation.
        """
        
        return np.array([np.array([bs.sim.simt, bs.traf.lat[i], bs.traf.lon[i], bs.traf.alt[i]*m, 
                                   bs.traf.hdg[i], bs.traf.cas[i]*ms]) for i in range(len(bs.traf.id))])

    def reset(self):
        """Reset the simulation environment.
        """
        
        # bs.sim.reset()
        bs.traf.reset()
        bs.sim.simt = 0
        bs.stack.stack(f'DT {self.dt}; FF')

