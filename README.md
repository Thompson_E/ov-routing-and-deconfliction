# OV Routing and Deconfliction
## Ellis Thompson
#### [thompson_e AT gwu dot edu](thompson_e@gwu.edu)

This project looks as both route deconfliction using operational volumes (OVs) as a strategic deconfliction method. On top of that a tactical deconfliction layer ensures flights remain safe while en route and within their OVs.

## Running the code
To run the test file the code can be ran with:
```
python main.py
```
Flags can be appended to the console command to customise the operation, see the table and example below:
| Flag                  | Description                                          | Type    | Default | Required |
|-----------------------|------------------------------------------------------|---------|---------|----------|
| `--routepath`, `-rte` | Path to a yaml file containing the route.            | `str`   | NONE    | No       |
| `--horizon`, `-th`    | Time horizon to project traces to in seconds.        | `int`   | 60      | No       |
| `--timedelta`,`-dt`   | How much time each step progresses the simulator by. | `float` | 1.0     | No       |
| `--noTraces`,`-n`     | How many traces will be generated in the simulation. | `int`   | 500     | No       |

Example:
```
python main.py -rte data/testrte.yml -th 40 -n 1000
```